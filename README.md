# Phase space tomography tool 

The collection of Python classes provide a tool to perform transverse phase space beam tomography. 

## Installation

To start, I suggest to create a dedicated virtual environment and activate it.

```bash
python -m venv /path/to/new/virtual/environment 
source /path/to/new/virtual/environment/bin/activate
```

#### Installation from Git

The package can be installed using pip as follows:

```bash
pip install git+https://gitlab.cern.ch/vbencini/phase_space_tomography.git 
```

#### Local installation

As an alternative the package can be installed locally after cloning the repository.

```bash
git clone https://gitlab.cern.ch/vbencini/phase_space_tomography.git
cd phase_space_tomography
pip install .
```

### Usage

A full working example can be found in the example folder, where the emittance reconstruction on the AWAKE 18 MeV line i showed. 

After installation, the package modules can be called as follows:

```python
import pstomo.preprocess
from pstomo.tomo_plot import Plot
from pstomo.reconstruct import Reconstruct
from pstomo.track import Lattice, Model, Track
... 
```
